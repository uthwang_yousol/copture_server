var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/login', function(req, res, next) {
  res.status(200).json({ 'userId': 'test_user:' + process.env.NODE_ENV})
});

router.get('/check-duplicate/email/:email', function(req, res, next) {
  res.status(200).json({ 'isDuplicate': 'true:' + process.env.NODE_ENV})
});

module.exports = router;

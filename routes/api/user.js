var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/:id', function(req, res, next) {
    res.status(200).json({ 'message': req.params.id + ' is not found:' + process.env.NODE_ENV})
});

module.exports = router;